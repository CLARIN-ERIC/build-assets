#!/bin/bash

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

#
# Validate user and directory
#
CWD="$(pwd)"
USER="$(whoami)"
if [ "${CWD}"  != "/home/deploy" ]; then
	echo "Run this script from: /home/deploy"
	exit 1
fi
if [ "${USER}" != "deploy" ]; then
	echo "Run this script as user: deploy"
	exit 1
fi

#
# Check build-asset versions
#
NOT_INSTALLED="Not installed"

CONTROL_TAG="${NOT_INSTALLED}"
if [ -d "${SCRIPTPATH}/control-script" ]; then
        CONTROL_TAG=$(. ~/.bashrc && cd "${SCRIPTPATH}/control-script" && sudo -u setup git describe --tags)
fi

DEPLOY_TAG="${NOT_INSTALLED}"
if [ -d "${SCRIPTPATH}/deploy-script" ]; then
        DEPLOY_TAG=$(cd "${SCRIPTPATH}/deploy-script" && sudo -u setup git describe --tags)
fi

echo "Build assets:"
echo "  Control script: ${CONTROL_TAG}"
echo "  Deploy script:  ${DEPLOY_TAG}"

#
# Check all projects
#
echo "Projects:"
COMPOSE_DIRS=$(find . -name 'clarin' -type d -exec dirname {} \;)
while read -r line; do
        if [ -f "${line}/clarin/docker-compose.yml" ]; then
                TAG=$(cd "${line}" && sudo -u setup git describe --tags)
                DIR=$(dirname "${line}")
		BASE=$(basename "${DIR}")

		echo "  ${BASE}: ${TAG} "
        fi
done <<< "${COMPOSE_DIRS}"